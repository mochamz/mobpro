<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use DB;

class BookCtrl extends Controller
{
	public function index()
	{
		$query = DB::table('m_book');
		$query->orderBy('title');
		$data = $query->get();

		$res['success'] = true;
		$res['result'] = $data;

		return response($res);
	}

	public function add(Request $request)
	{
		$title = ($request->input("title") != "") ? $request->input("title") : "";
		$author = ($request->input("author") != "") ? $request->input("author") : "";
		$publisher = ($request->input("publisher") != "") ? $request->input("publisher") : "";

		$data = [
		"title" => $title,
		"author" => $author,
		"publisher" => $publisher
		];

		DB::beginTransaction();
		try {
			DB::table('m_book')->insert($data);
			DB::commit();
			$res['success'] = 1;
			$res['message'] = "Data berhasil di simpan";
		} catch (QueryException $e) {
			DB::rollback();
			$res['success'] = 0;
			$res['message'] = "Nama buku sudah digunakan";
		}

		return response($res);
	}

	public function edit(Request $request)
	{
		$id = ($request->input("id") != "") ? $request->input("id") : "";

		$query = DB::table('m_book');
		$query->where('id', $id);
		$data = $query->first();
		if ($data) {
			$res['success'] = 1;
			$res['id'] = $data->id;
			$res['title'] = $data->title;
			$res['author'] = $data->author;
			$res['publisher'] = $data->publisher;
		} else {
			$res['success'] = 0;
			$res['message'] = "Error Mengambil Data";
		}

		return response($res);
	}

	public function update(Request $request)
	{
		$id = ($request->input("id") != "") ? $request->input("id") : "";
		$title = ($request->input("title") != "") ? $request->input("title") : "";
		$author = ($request->input("author") != "") ? $request->input("author") : "";
		$publisher = ($request->input("publisher") != "") ? $request->input("publisher") : "";

		$data = [
		"title" => $title,
		"author" => $author,
		"publisher" => $publisher
		];

		DB::beginTransaction();
		try {
			DB::table('m_book')->where('id', $id)->update($data);
			DB::commit();
			$res['success'] = 1;
			$res['message'] = "Data berhasil di update";
		} catch (QueryException $e) {
			DB::rollback();
			$res['success'] = 0;
			$res['message'] = "Nama buku sudah digunakan";
		}

		return response($res);
	}

	public function delete(Request $request)
	{
		$id = ($request->input("id") != "") ? $request->input("id") : "";

		DB::beginTransaction();
		try {
			DB::table('m_book')->where('id', $id)->delete();
			DB::commit();
			$res['success'] = 1;
			$res['message'] = "Data berhasil di hapus";
		} catch (QueryException $e) {
			DB::rollback();
			$res['success'] = 0;
			$res['result'] = $e;
		}

		return response($res);
	}
}