<?php

use Illuminate\Database\Seeder;

class MBookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
        [
        "title" => "Buku1",
        "author" => "Halili",
        "publisher" => "Gramedia",
        ],
        [
        "title" => "Buku2",
        "author" => "Bagus",
        "publisher" => "Andi Publisher",
        ],
        [
        "title" => "Buku3",
        "author" => "Warsono",
        "publisher" => "Paradoks",
        ],
        [
        "title" => "Buku4",
        "author" => "Halili",
        "publisher" => "Tiga Serangkai",
        ],
        ];

        foreach ($data as $row) {
            DB::table('m_book')->insert($row);
        }
    }
}
