<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('/key', function() {
    return str_random(32);
});

$app->get('/book', 'BookCtrl@index');
$app->post('/book-add', 'BookCtrl@add');
$app->post('/book-edit', 'BookCtrl@edit');
$app->post('/book-update', 'BookCtrl@update');
$app->post('/book-delete', 'BookCtrl@delete');
